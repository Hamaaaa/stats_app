export class Event {
  constructor(eventType, project_name) {
    this.eventType = eventType;
    this.projectName = project_name;
    this.apiUrl = "https://stats-api.onrender.com/api/events/add";
  }

  async addEventToDatabase() {
    try {
      const eventData = {
        event_type: this.eventType,
        project_name: this.projectId,
      };

      const response = await fetch(this.apiUrl, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(eventData),
      });
      console.log(response);
      if (response.status === 200) {
        console.log("Event added successfully.");
      } else {
        console.log("Failed to add the event.");
      }
    } catch (error) {
      console.log(error);
      console.error("Error adding event to the database:", error.message);
    }
  }
}
