import { Link, useNavigate } from "react-router-dom";
import "./login.scss";
import { useContext, useState } from "react";
import axios from "axios";
import { ToastContainer } from "react-toastify";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { Context } from "../../context/Context";
import { useCookies } from "react-cookie";

const Login = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [cookies, setCookie] = useCookies(["myCookie"]);
  const [password, setPassword] = useState("");
  const { handleSuccess, handleError } = useContext(Context);

  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      const { data } = await axios.post(
        "https://stats-api.onrender.com/api/auth/login",
        {
          email,
          password,
        }
      );
      const { status, msg } = data;
      if (status === "success") {
        setCookie("token", data.token, { maxAge: 3600 });
        localStorage.setItem("token", data.token);
        handleSuccess(msg);
        setTimeout(() => {
          navigate("/");
        }, 4000);
      } else {
        handleError(msg);
      }
    } catch (err) {
      handleError("Server Error");
    }
  };
  return (
    <div className="login">
      <div className="left"></div>
      <div className="right">
        <div className="title">
          <AccountCircleIcon className="icon" />
          <h2>Welcome!</h2>
        </div>
        <div className="formContainer">
          <form method="POST" className="form" onSubmit={handleLogin}>
            <input
              type="email"
              placeholder="Email"
              name="email"
              onChange={(e) => setEmail(e.target.value)}
            />
            <input
              type="password"
              placeholder="Password"
              name="password"
              onChange={(e) => setPassword(e.target.value)}
            />
            <button type="submit" className="button-confirm">
              Login
            </button>
            <h3>
              Create new account{" "}
              <Link to="/signup" className="link">
                Here
              </Link>
            </h3>
          </form>
        </div>
      </div>
      <ToastContainer
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick={false}
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover={false}
      />
    </div>
  );
};

export default Login;
