import React, { useContext, useEffect } from "react";
import "./home.scss";
import Sidebar from "../../components/Sidebar/Sidebar";
import Navbar from "../../components/Navbar/Navbar";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import Profile from "../../components/Profile/Profile";
import { Context } from "../../context/Context";
import List from "../../components/List/List";
import Admins from "../../components/Admins/Admins";
import Events from "../../components/Events/Events";

const Home = () => {
  const { sideBar } = useContext(Context);
  const navigate = useNavigate();
  const [cookies, removeCookie] = useCookies([]);

  useEffect(() => {
    const verifyCookie = async () => {
      if (!cookies.token || cookies.token === "undefined") {
        navigate("/login");
      }
    };

    verifyCookie();
  }, [cookies, navigate, removeCookie]);

  return (
    <div className="home">
      <Sidebar />
      <div className="homeContainer">
        <Navbar />
        {sideBar[0]?.selected && <Profile />}
        {sideBar[1]?.selected && <List />}
        {sideBar[2]?.selected && <Events />}
        {sideBar[4]?.selected && <Admins />}
      </div>
    </div>
  );
};

export default Home;
