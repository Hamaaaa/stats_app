import React, { useState } from "react";
import "./signup.scss";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { ToastContainer, toast } from "react-toastify";
import { useForm } from "react-hook-form";

const Signup = () => {
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleError = (err) =>
    toast.error(err, {
      position: "bottom-left",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
    });

  const handleSuccess = (msg) =>
    toast.success(msg, {
      position: "bottom-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
    });

  const handleRegister = async (e) => {
    e.preventDefault();
    try {
      const { data } = await axios.post(
        "https://stats-api.onrender.com/api/auth/signup",
        {
          username,
          email,
          password,
        }
      );
      const { status, msg } = data;
      if (status === "success") {
        handleSuccess(msg);
        setTimeout(() => {
          navigate("/login");
        }, 6000);
      } else {
        handleError(msg);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="signup">
      <div className="left"></div>
      <div className="right">
        <div className="title">
          <AccountCircleIcon className="icon" />
          <h2>Welcome!</h2>
        </div>
        <div className="formContainer">
          <form method="POST" className="form" onSubmit={handleRegister}>
            <input
              type="text"
              placeholder="Username"
              name="username"
              onChange={(e) => setUsername(e.target.value)}
            />
            <input
              type="email"
              placeholder="Email"
              name="email"
              onChange={(e) => setEmail(e.target.value)}
            />
            <input
              type="password"
              placeholder="Password"
              name="password"
              onChange={(e) => setPassword(e.target.value)}
            />
            <button type="submit" className="button-confirm">
              Register
            </button>
            <h3>
              You already have an account{" "}
              <Link to="/login" className="link">
                Login Here
              </Link>
            </h3>
          </form>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default Signup;
