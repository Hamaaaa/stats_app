import React, { useContext } from "react";
import "./sidebar.scss";
import { useCookies } from "react-cookie";
import { toast, ToastContainer } from "react-toastify";
import { useNavigate } from "react-router-dom";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { Context } from "../../context/Context";

const Sidebar = () => {
  const { sideBar, handleSidebar } = useContext(Context);
  const [cookies, removeCookie] = useCookies([]);
  const navigate = useNavigate();
  const handleLogout = async () => {
    removeCookie("token");
    localStorage.removeItem("token");
    toast.info("Logged out successfully!", {
      position: "bottom-right",
    });
    setTimeout(() => {
      navigate("/login");
    }, 6000);
  };

  const showLogout = () => {
    let ele = document.querySelector(".logout");
    ele.classList.toggle("toggleLog");
  };

  return (
    <div className="sidebar active">
      <div className="top">
        <h1 className="logo">TAKOLOR</h1>
      </div>
      <div className="center">
        <ul>
          {sideBar?.map((item) => (
            <li
              key={item.id}
              className={item.selected ? "selected" : ""}
              onClick={handleSidebar}
            >
              <a href="#">{item.name}</a>
            </li>
          ))}
        </ul>
      </div>
      <div className="bottom">
        <AccountCircleIcon className="icon" onClick={showLogout} />
        <button className="logout" onClick={handleLogout}>
          Logout
        </button>
      </div>
      <ToastContainer />
    </div>
  );
};

export default Sidebar;
