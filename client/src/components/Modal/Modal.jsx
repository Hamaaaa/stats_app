import React, { useContext, useState } from "react";
import "./modal.scss";
import { Context } from "../../context/Context";
import axios from "axios";

const Modal = ({ setShowAdd }) => {
  const [project_name, setProjectName] = useState("");
  const [description, setDescription] = useState("");
  const { handleError, handleSuccess } = useContext(Context);

  const handleSave = async () => {
    try {
      const { data } = await axios.post(
        "https://stats-api.onrender.com/api/projects/add",
        { project_name, description },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      const { status, msg } = data;
      if (status === "success") {
        handleSuccess(msg);
      }
    } catch (err) {
      handleError("Server Error");
    }
  };
  return (
    <div className="modal">
      <div className="modal-title">Add a Project</div>
      <label>Project Name :</label>
      <input
        type="text"
        placeholder="Project Name"
        onChange={(e) => setProjectName(e.target.value)}
      />
      <label>Project Description :</label>
      <input
        type="text"
        placeholder="Description"
        onChange={(e) => setDescription(e.target.value)}
      />
      <div className="buttons">
        <button onClick={handleSave}>Save Project</button>
        <button onClick={() => setShowAdd(false)}>Cancel</button>
      </div>
    </div>
  );
};

export default Modal;
