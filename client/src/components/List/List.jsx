import React, { useState } from "react";
import "./list.scss";
import Modal from "../Modal/Modal";
import Projects from "../Projects/Projects";

const List = () => {
  const [showAdd, setShowAdd] = useState(false);
  const [showProjects, setShowProjects] = useState(false);
  const handleAdd = () => {
    setShowAdd(true);
  };
  return showProjects ? (
    <Projects setShowProjects={setShowProjects} />
  ) : (
    <div className="list">
      <button className="btn" onClick={handleAdd}>
        <span>Add a Project</span>
      </button>
      <button className="btn" onClick={() => setShowProjects(true)}>
        <span>Browser Your Projects</span>
      </button>
      {showAdd && (
        <>
          <div className="overlay" onClick={() => setShowAdd(false)}></div>
          <Modal setShowAdd={setShowAdd} />
        </>
      )}
    </div>
  );
};

export default List;
