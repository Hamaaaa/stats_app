import "./navbar.scss";
import MenuIcon from "@mui/icons-material/Menu";
import MarkUnreadChatAltIcon from "@mui/icons-material/MarkUnreadChatAlt";

const Navbar = () => {
  const handleMenu = () => {
    let sidebar = document.querySelector(".sidebar");
    sidebar.classList.toggle("active");
  };
  return (
    <div className="navbar">
      <div className="wrapper">
        <div className="left">
          <MenuIcon className="icon" onClick={handleMenu} />
          <h2>DASHBOARD</h2>
        </div>
        <div className="right">
          <MarkUnreadChatAltIcon className="icon" />
          <h2>NOTIFICATIONS</h2>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
