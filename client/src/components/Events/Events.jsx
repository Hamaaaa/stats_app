import { useContext, useEffect, useState } from "react";
import "./events.scss";
import { Context } from "../../context/Context";
import Event from "./Event/Event";

const Events = () => {
  const { handleProjects, projects } = useContext(Context);
  const [showEvents, setShowEvents] = useState(false);
  const [project, setProject] = useState({});

  const handleEvent = (project_name, project_id) => {
    setProject({
      project_name,
      project_id,
    });
    setShowEvents(true);
  };
  useEffect(() => {
    handleProjects();
  }, []);

  return (
    <>
      {showEvents ? (
        <Event
          project_name={project.project_name}
          project_id={project.project_id}
          setShowEvents={setShowEvents}
        />
      ) : (
        <div className="events">
          <div className="title">
            <h1>Select a Project to see the Events</h1>
          </div>
          <div className="projects">
            {projects?.map((project) => (
              <h3
                key={project._id}
                onClick={() => handleEvent(project.project_name, project._id)}
              >
                {project.project_name}
              </h3>
            ))}
          </div>
        </div>
      )}
    </>
  );
};

export default Events;
