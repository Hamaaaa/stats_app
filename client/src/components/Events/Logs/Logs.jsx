import { useEffect, useState } from "react";
import "./logs.scss";
import { Box, Typography } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import axios from "axios";
import moment from "moment";

const Logs = ({ setShowEvent, eventLogs }) => {
  const [logs, setLogs] = useState([]);
  const handleLogs = async () => {
    try {
      let { data } = await axios.post(
        "https://stats-api.onrender.com/api/events/logs",
        {
          logIds: eventLogs,
        }
      );
      const { status, logs } = data;
      if (status === "success") {
        setLogs(logs);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const columns = [
    {
      field: "log_ip",
      headerName: "Log IP",
      width: 200,
      editable: false,
    },
    {
      field: "email",
      headerName: "Email",
      width: 250,
      editable: false,
    },
    {
      field: "createdAt",
      headerName: "Created At",
      width: 200,
      renderCell: (params) =>
        moment(params.row.createdAt).format("YYYY-MM-DD HH:MM:SS"),
    },
    {
      field: "_id",
      headerName: "Id",
      width: 250,
      sortable: false,
      filterable: false,
    },
    {
      field: "event_id",
      headerName: "Event ID",
      width: 250,
      sortable: false,
      filterable: false,
    },
  ];

  useEffect(() => {
    handleLogs();
  }, []);

  return (
    <div className="logs">
      <Box sx={{ height: 400, width: "100%" }}>
        <Typography
          variant="h3"
          component="h3"
          sx={{ textAlign: "center", mt: 3, mb: 3 }}
        >
          Logs
        </Typography>
        <DataGrid columns={columns} rows={logs} getRowId={(row) => row._id} />
      </Box>
      <button className="btn" onClick={() => setShowEvent(false)}>
        Return
      </button>
    </div>
  );
};

export default Logs;
