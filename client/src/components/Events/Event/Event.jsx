import { useEffect, useState } from "react";
import "./event.scss";
import axios from "axios";
import Chart from "../../Chart/Chart";
import Logs from "../Logs/Logs";

const Event = ({ project_name, project_id, setShowEvents }) => {
  const [events, setEvents] = useState([]);
  const [showEvent, setShowEvent] = useState(false);
  const [currEvent, setCurrEvent] = useState({});
  const [showChart, setShowChart] = useState(false);

  const handleEvents = async () => {
    try {
      const { data } = await axios.get(
        `https://stats-api.onrender.com/api/events?id=${project_id}`
      );
      const { events, msg } = data;
      if (msg === "success") {
        setEvents(events);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const handleEvent = (event) => {
    setCurrEvent(event);
    setShowEvent(true);
  };

  useEffect(() => {
    handleEvents();
  }, []);
  return (
    <>
      {showEvent ? (
        <Logs setShowEvent={setShowEvent} eventLogs={currEvent.logs} />
      ) : showChart ? (
        <Chart setShowChart={setShowChart} events={events} />
      ) : (
        <div className="event">
          <h1>This is {project_name} Events</h1>
          {events.length === 0 ? (
            <h3 style={{ marginTop: "50px" }}>No Events</h3>
          ) : (
            <div className="content">
              {events?.map((event) => (
                <div
                  key={event._id}
                  className="event_item"
                  onClick={() => handleEvent(event)}
                >
                  <a href="#" className="event-item_link">
                    <div className="event-item_bg"></div>
                    <div className="event-item_title">{event?.event_type}</div>
                    <div className="counter">
                      <h3>Counter: {event?.counter}</h3>
                    </div>
                  </a>
                </div>
              ))}
            </div>
          )}
          <div className="btns">
            {events.length > 0 && (
              <button className="btn" onClick={() => setShowChart(true)}>
                View Chart
              </button>
            )}
            <button className="btn" onClick={() => setShowEvents(false)}>
              Return
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default Event;
