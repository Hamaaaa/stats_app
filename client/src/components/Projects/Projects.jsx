import React, { useContext, useEffect, useState } from "react";
import "./projects.scss";
import { Box, Button, Typography } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import { ToastContainer } from "react-toastify";
import axios from "axios";
import { Context } from "../../context/Context";
import moment from "moment";

const Projects = ({ setShowProjects }) => {
  const [pageSize, setPageSize] = useState(5);
  const [deletedProjects, setDeletedProjects] = useState([]);
  const { handleError, handleSuccess, handleProjects, projects, setProjects } =
    useContext(Context);
  const [changedProjects, setChangedProjects] = useState([]);

  const handleDelete = (id) => {
    const updatedRows = projects.filter((project) => project._id !== id);
    setDeletedProjects((prev) => [...prev, id]);
    setProjects(updatedRows);
  };

  const handleProcessRowUpdateError = () => {};

  const handleEvent = (row) => {
    let checkPresent = changedProjects.find(
      (project) => project._id === row._id
    );
    if (!checkPresent) {
      setChangedProjects((prev) => [...prev, row]);
    } else {
      setChangedProjects((prev) => {
        return prev.map((project) => {
          if (project._id === row._id) {
            return row;
          } else {
            return project;
          }
        });
      });
    }
  };

  const handleSave = async () => {
    try {
      if (deletedProjects.length !== 0) {
        await axios.post(
          "https://stats-api.onrender.com/api/projects/delete",
          { deletedProjects },
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        );
        setDeletedProjects([]);
      }
      if (changedProjects.length !== 0) {
        await axios.put(
          "https://stats-api.onrender.com/api/projects/update",
          { changedProjects },
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        );
        setChangedProjects([]);
      }
      handleSuccess("Save Successfully!!");
    } catch (err) {
      handleError("Unknown Error");
      console.log(err);
    }
  };

  const columns = [
    {
      field: "project_name",
      headerName: "Project Name",
      width: 200,
      editable: true,
    },
    {
      field: "description",
      headerName: "Description",
      width: 350,
      editable: true,
    },
    {
      field: "createdAt",
      headerName: "Created At",
      width: 200,
      renderCell: (params) =>
        moment(params.row.createdAt).format("YYYY-MM-DD HH:MM:SS"),
    },
    {
      field: "_id",
      headerName: "Id",
      width: 250,
      sortable: false,
      filterable: false,
    },
    {
      field: "delete",
      headerName: "",
      width: 100,
      renderCell: (params) => (
        <Button
          variant="outlined"
          color="secondary"
          onClick={() => handleDelete(params.id)}
        >
          Delete
        </Button>
      ),
      sortable: false,
      filterable: false,
    },
  ];

  useEffect(() => {
    handleProjects();
  }, []);
  return (
    <>
      <div className="projects">
        <Box sx={{ height: 400, width: "100%" }}>
          <Typography
            variant="h3"
            component="h3"
            sx={{ textAlign: "center", mt: 3, mb: 3 }}
          >
            Manage Your Projects
          </Typography>
          <DataGrid
            columns={columns}
            rows={projects}
            getRowId={(row) => row._id}
            rowsPerPageOptions={[5, 10, 20]}
            pageSize={pageSize}
            onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
            processRowUpdate={(updatedRow, originalRow) =>
              handleEvent(updatedRow)
            }
            onProcessRowUpdateError={handleProcessRowUpdateError}
          />
        </Box>
        <div className="buttons">
          <button className="btn" onClick={handleSave}>
            Save Changes
          </button>
          <button className="btn" onClick={() => setShowProjects(false)}>
            Return
          </button>
        </div>
      </div>
      <ToastContainer />
    </>
  );
};

export default Projects;
