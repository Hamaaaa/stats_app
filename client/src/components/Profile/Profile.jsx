import { useContext, useEffect, useState } from "react";
import "./profile.scss";
import axios from "axios";
import { Context } from "../../context/Context";
import { ToastContainer } from "react-toastify";

const Profile = () => {
  const { user, handleSuccess, handleError } = useContext(Context);
  const [updatedUser, setUpdatedUser] = useState(user);
  const [oldPass, setOldPass] = useState("");
  const [newPass, setNewPass] = useState("");

  const handlePassword = async () => {
    try {
      const { data } = await axios.put(
        "https://stats-api.onrender.com/api/admins/updatePassword",
        { current_password: oldPass, new_password: newPass },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      const { status, msg } = data;
      if (status === "success") {
        handleSuccess(msg);
        setNewPass("");
        setOldPass("");
      } else {
        handleError(msg);
      }
    } catch (err) {
      handleError("Server Error");
    }
  };

  const handleUpdate = async () => {
    try {
      await axios.put(
        "https://stats-api.onrender.com/api/admins/update",
        updatedUser,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      handleSuccess("Admin Updated Successfully!");
    } catch (err) {
      handleError("Server Error!");
    }
  };

  return (
    <div className="profile">
      <div className="container">
        <div className="edit">
          <h2>Edit Profile</h2>
          <div className="formContent">
            <div className="item">
              <label>Username:</label>
              <input
                type="text"
                placeholder={user?.username}
                onChange={(e) =>
                  setUpdatedUser({ ...updatedUser, username: e.target.value })
                }
              />
            </div>
            <div className="item">
              <label>Email:</label>
              <input
                type="text"
                placeholder={user?.email}
                onChange={(e) =>
                  setUpdatedUser({ ...updatedUser, email: e.target.value })
                }
              />
            </div>
            <div className="item">
              <label>Role:</label>
              <input type="text" placeholder={user?.role} disabled />
            </div>
          </div>
          <button onClick={handleUpdate}>Save Changes</button>
        </div>
        <div className="line"></div>
        <div className="editPass">
          <h2>Edit Password</h2>
          <div className="formContent">
            <input
              type="password"
              placeholder="Enter your current Password"
              value={oldPass}
              onChange={(e) => setOldPass(e.target.value)}
            />
            <input
              type="password"
              placeholder="Enter the new Password"
              value={newPass}
              onChange={(e) => setNewPass(e.target.value)}
            />
            <button onClick={handlePassword}>Save Password</button>
          </div>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default Profile;
