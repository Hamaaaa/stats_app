import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import "./chart.scss";

const Chart = ({ setShowChart, events }) => {
  const data = events.map((event) => {
    return {
      name: event.event_type,
      Counter: event.counter,
    };
  });

  return (
    <div className="chart">
      <LineChart
        width={800}
        height={400}
        data={data}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line
          type="monotone"
          dataKey="Counter"
          stroke="#8884d8"
          activeDot={{ r: 8 }}
        />
      </LineChart>
      <button
        className="btn"
        onClick={() => setShowChart(false)}
        style={{ marginTop: "40px" }}
      >
        Return
      </button>
    </div>
  );
};

export default Chart;
