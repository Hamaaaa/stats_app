import React, { useContext, useEffect, useMemo, useState } from "react";
import "./admins.scss";
import { Box, Typography } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import axios from "axios";
import moment from "moment";
import Button from "@mui/material/Button";
import { Context } from "../../context/Context";
import { ToastContainer } from "react-toastify";

const Admins = () => {
  const [admins, setAdmins] = useState([]);
  const [pageSize, setPageSize] = useState(5);
  const [deletedAdmins, setDeletedAdmins] = useState([]);
  const [changedAdmins, setChangedAdmins] = useState([]);
  const { handleSuccess, handleError } = useContext(Context);

  const handleDelete = (id) => {
    const updatedRows = admins.filter((admin) => admin._id !== id);
    setDeletedAdmins((prev) => [...prev, id]);
    setAdmins(updatedRows);
  };

  const handleEvent = (params) => {
    const { id, formattedValue } = params;
    const checkPresent = changedAdmins.find((admin) => admin.id === id);
    if (checkPresent) {
      setChangedAdmins(
        changedAdmins.map((admin) => {
          if (admin.id === id) {
            admin.role = admin.role === "basic" ? "super" : "basic";
          }
          return admin;
        })
      );
    } else {
      setChangedAdmins((prev) => [
        ...prev,
        { id, role: formattedValue === "basic" ? "super" : "basic" },
      ]);
    }
  };

  const handleAdmins = async () => {
    try {
      const res = await axios.get("https://stats-api.onrender.com/api/admins", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      setAdmins(res.data.admins);
    } catch (err) {
      console.log(err);
    }
  };

  const handleSave = async () => {
    try {
      if (deletedAdmins.length !== 0) {
        await axios.post(
          "https://stats-api.onrender.com/api/admins/delete",
          { deletedAdmins },
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        );
        setDeletedAdmins([]);
      }
      if (changedAdmins.length !== 0) {
        await axios.post(
          "https://stats-api.onrender.com/api/admins/updateRole",
          { changedAdmins },
          {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          }
        );
        setChangedAdmins([]);
      }
      handleSuccess("Save Successfully!!");
    } catch (err) {
      handleError("Unknown Error");
    }
  };

  const columns = [
    { field: "username", headerName: "Username", width: 200 },
    { field: "email", headerName: "Email", width: 200 },
    {
      field: "createdAt",
      headerName: "Created At",
      width: 200,
      renderCell: (params) =>
        moment(params.row.createdAt).format("YYYY-MM-DD HH:MM:SS"),
    },
    {
      field: "_id",
      headerName: "Id",
      width: 250,
      sortable: false,
      filterable: false,
    },
    {
      field: "role",
      headerName: "Role",
      width: 100,
      type: "singleSelect",
      valueOptions: ["basic", "super"],
      editable: true,
      sortable: false,
      filterable: false,
    },
    {
      field: "delete",
      headerName: "",
      width: 100,
      renderCell: (params) => (
        <Button
          variant="outlined"
          color="secondary"
          onClick={() => handleDelete(params.id)}
        >
          Delete
        </Button>
      ),
      sortable: false,
      filterable: false,
    },
  ];

  useEffect(() => {
    handleAdmins();
  }, []);

  return (
    <div className="admins">
      <Box sx={{ height: 400, width: "100%" }}>
        <Typography
          variant="h3"
          component="h3"
          sx={{ textAlign: "center", mt: 3, mb: 3 }}
        >
          Manage Admins
        </Typography>
        <DataGrid
          columns={columns}
          rows={admins}
          getRowId={(row) => row._id}
          rowsPerPageOptions={[5, 10, 20]}
          pageSize={pageSize}
          onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
          onCellEditStop={handleEvent}
        />
      </Box>
      <button className="saveBtn" onClick={handleSave}>
        Save Changes
      </button>
      <ToastContainer />
    </div>
  );
};
export default Admins;
