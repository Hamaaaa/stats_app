import { createContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";

export const Context = createContext();

export const ContextProvider = ({ children }) => {
  const [user, setUser] = useState({});
  const [role, setRole] = useState("");
  const [projects, setProjects] = useState([]);

  const handleProjects = async () => {
    try {
      const { data } = await axios.get(
        "https://stats-api.onrender.com/api/projects",
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      if (data.status === "success") {
        setProjects(data.projects);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const handleError = (err) =>
    toast.error(err, {
      position: "bottom-left",
    });
  const handleSuccess = (msg) =>
    toast.success(msg, {
      position: "bottom-left",
    });

  const getProfile = async () => {
    try {
      const res = await axios.get(
        "https://stats-api.onrender.com/api/admins/profile",
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      setUser({
        username: res.data.username,
        email: res.data.email,
        role: res.data.role,
      });
      setRole(res.data.role);
    } catch (err) {
      console.log(err);
    }
  };

  const [sideBar, setSideBar] = useState([
    { id: 0, name: "PROFILE", selected: false },
    { id: 1, name: "PROJECTS", selected: false },
    { id: 2, name: "EVENTS", selected: false },
  ]);

  const handleSidebar = (e) => {
    const name = e.target.textContent;
    setSideBar(
      sideBar.map((item) => {
        if (item.name === name) {
          item.selected = true;
        } else {
          item.selected = false;
        }
        return item;
      })
    );
  };
  useEffect(() => {
    getProfile();
  }, [localStorage.getItem("token")]);

  useEffect(() => {
    if (role === "super") {
      setSideBar([
        { id: 0, name: "PROFILE", selected: false },
        { id: 1, name: "PROJECTS", selected: false },
        { id: 2, name: "EVENTS", selected: false },
        { id: 4, name: "ADMINS", selected: false },
      ]);
    }
  }, [role]);
  return (
    <Context.Provider
      value={{
        user,
        sideBar,
        setSideBar,
        handleSidebar,
        handleSuccess,
        handleError,
        handleProjects,
        setProjects,
        projects,
      }}
    >
      {children}
    </Context.Provider>
  );
};
