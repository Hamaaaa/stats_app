const express = require("express");
const dotenv = require("dotenv");
const connectDB = require("./connection");
const cookieParser = require("cookie-parser");
const authRouter = require("./routes/authRoutes");
const projectRouter = require("./routes/projectRoutes");
const adminRouter = require("./routes/adminRoutes");
const eventRouter = require("./routes/eventRoutes");
const cors = require("cors");

const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());
app.options("*", cors());

dotenv.config();
connectDB();
app.use(cookieParser());
app.use("/api/auth", authRouter);
app.use("/api/projects", projectRouter);
app.use("/api/admins", adminRouter);
app.use("/api/events", eventRouter);

app.listen(port, () => {
  console.log(`App running on port ${port}`);
});
