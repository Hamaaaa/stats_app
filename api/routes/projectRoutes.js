const express = require("express");
const projectController = require("../controllers/projectController");
const authController = require("../controllers/authController");
const superAdminController = require("../controllers/superAdminController");

const router = express.Router();

router.use(authController.protect);
router.put(
  "/add-admin",
  authController.restrictTo,
  superAdminController.addAdmin
);
router.get("/", projectController.getAllProjects);
router.get("/project", projectController.getProject);
router.post("/add", projectController.addProject);
router.post("/delete", projectController.deleteProject);
router.put("/update", projectController.updateProjects);

module.exports = router;
