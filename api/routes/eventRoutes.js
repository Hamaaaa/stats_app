const express = require("express");
const eventController = require("../controllers/eventController");

const router = express.Router();

router.post("/add", eventController.add);
router.get("/", eventController.getAll);
router.post("/logs", eventController.getLogs);

module.exports = router;
