const express = require("express");
const authController = require("../controllers/authController");
const adminController = require("../controllers/adminController");
const superAdminController = require("../controllers/superAdminController");

const router = express.Router();

router.use(authController.protect);
router.get("/", authController.restrictTo, superAdminController.getAllAdmins);
router.get("/profile", adminController.getAdminProfile);
router.put("/update", adminController.updateAdmin);
router.put("/updatePassword", adminController.updatePassword);
router.post(
  "/delete",
  authController.restrictTo,
  superAdminController.deleteAdmins
);
router.post(
  "/updateRole",
  authController.restrictTo,
  superAdminController.updateRole
);
router.put("/exclude", authController.restrictTo, superAdminController.exclude);

module.exports = router;
