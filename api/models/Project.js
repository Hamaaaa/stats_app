const mongoose = require("mongoose");

const ProjectSchema = mongoose.Schema(
  {
    project_name: {
      type: String,
      require: true,
      unique: true,
    },
    description: {
      type: String,
    },
    admins_id: {
      type: Array,
      require: true,
    },
    events: {
      type: Array,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Project", ProjectSchema);
