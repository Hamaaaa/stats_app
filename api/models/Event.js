const mongoose = require("mongoose");

const EventSchema = mongoose.Schema(
  {
    event_type: {
      type: String,
      require: true,
    },
    counter: {
      type: Number,
      default: 1,
    },
    project_id: {
      type: String,
      require: true,
    },
    logs: {
      type: Array,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Event", EventSchema);
