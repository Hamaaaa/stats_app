const Admin = require("../models/Admin");
const Project = require("../models/Project");
const mongoose = require("mongoose");

exports.addAdmin = async (req, res) => {
  const { admin_id, project_id } = req.body;
  let project = await Project.findById(project_id);
  if (!project) {
    return res.status(404).json({
      msg: "Project Not Found!!",
    });
  }
  const admins = project.admins_id.map((item) => item.toString());
  if (admins.includes(admin_id)) {
    return res.status(501).json({
      msg: "Admin Already Belong to this project",
    });
  } else {
    admin_is = new mongoose.Types.ObjectId(admin_id);
    project.admins_id.push(admin_is);
    await project.save();
    return res.status(200).json({
      msg: "Admin Added Successfully",
    });
  }
};

exports.getAllAdmins = async (req, res) => {
  const admins = await Admin.find().select("-password");
  return res.status(200).json({
    status: "success",
    length: admins.length,
    admins,
  });
};

exports.deleteAdmins = async (req, res) => {
  const { deletedAdmins } = req.body;
  try {
    const deleteResult = await Admin.deleteMany({
      _id: { $in: deletedAdmins },
    });
    if (!deleteResult) {
      return res.status(404).json({
        msg: "Admins are not found",
      });
    }
    return res.status(200).json({
      msg: `${deleteResult.deletedCount} Admin(s) deleted successfully`,
    });
  } catch (err) {
    return res.status(501).json({
      msg: err,
    });
  }
};

exports.updateRole = async (req, res) => {
  let { changedAdmins } = req.body;
  try {
    for (const { id, role } of changedAdmins) {
      await Admin.findByIdAndUpdate(id, { role });
    }
    return res
      .status(200)
      .json({ status: "success", msg: "Admins Role Updated Successfully!!" });
  } catch (err) {
    return res.status(501).json({ status: "Failed", msg: "Internal Error" });
  }
};

exports.exclude = async (req, res) => {
  let { admin_id, project_name } = req.body;
  const project = await Project.findOne({ project_name });
  if (!project) {
    return res.status(404).json({
      msg: "Project not found",
    });
  }
  admin_id = new mongoose.Types.ObjectId(admin_id);
  if (!project.admins_id.includes(admin_id)) {
    return res.status(501).json({
      msg: "Admin Already Not included in this Project",
    });
  }
  project.admins_id = project.admins_id.filter(
    (item) => item.toString() !== admin_id.toString()
  );
  try {
    await project.save();
    return res.status(200).json({
      msg: "Admin Excluded Successfully",
    });
  } catch (err) {
    res.status(502).json(err);
  }
};
