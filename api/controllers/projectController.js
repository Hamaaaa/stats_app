const Project = require("../models/Project");

exports.getProject = async (req, res) => {
  const project = await Project.findOne({
    project_name: req.body.project_name,
  });
  if (!project) {
    return res.status(404).json({ msg: "Project Not Found" });
  }
  const adminsId = project.admins_id;
  if (adminsId.includes(req.admin._id) || req.admin.role === "super") {
    return res.status(200).json({
      ...project.toObject(),
      admins_id: undefined,
    });
  }
  return res
    .status(401)
    .json({ msg: "This Project Belongs to an other admin!!" });
};

exports.getAllProjects = async (req, res) => {
  let projects;
  try {
    if (req.admin.role === "super") {
      projects = await Project.find().select("-admins_id");
      return res.status(200).json({
        status: "success",
        length: projects.length,
        projects,
      });
    }
    projects = await Project.find({
      admins_id: req.admin._id,
    }).select("-admins_id");
    if (projects.length === 0) {
      return res.json({ status: "failed", msg: "You have no Projects" });
    }

    return res.status(200).json({
      status: "success",
      projects,
    });
  } catch (err) {
    res.status(501).json({
      status: "failed",
      msg: "Server Error",
    });
  }
};

exports.addProject = async (req, res) => {
  const project = new Project({
    project_name: req.body.project_name,
    description: req.body.description,
    admins_id: req.admin._id,
  });
  try {
    await project.save();
    return res.status(200).json({
      ...project.toObject(),
      status: "success",
      msg: "Project Added Successfully",
      admins_id: undefined,
    });
  } catch (err) {
    return res.status(501).json({
      status: "failed",
      msg: "Internal Error",
    });
  }
};

exports.updateProjects = async (req, res) => {
  const { changedProjects } = req.body;
  try {
    for (const { _id, project_name, description } of changedProjects) {
      await Project.findByIdAndUpdate(_id, { project_name, description });
    }
    return res
      .status(200)
      .json({ status: "success", msg: "Projects Updated Successfully!!" });
  } catch (err) {
    return res.status(501).json({ status: "Failed", msg: "Internal Error" });
  }
};

exports.deleteProject = async (req, res) => {
  const { deletedProjects } = req.body;
  try {
    const deleteResult = await Project.deleteMany({
      _id: { $in: deletedProjects },
    });
    if (!deleteResult) {
      return res.json({
        status: "failed",
        msg: "Projects not found",
      });
    }
    return res.status(200).json({
      status: "success",
      msg: `${deleteResult.deletedCount} Project(s) deleted successfully`,
    });
  } catch (err) {
    return res.status(501).json({
      status: "failed",
      msg: err,
    });
  }
};
