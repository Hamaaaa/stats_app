const Event = require("../models/Event");
const Logger = require("../models/Logger");
const Project = require("../models/Project");

exports.add = async (req, res) => {
  const { event_type, project_name } = req.body;
  let ip = req.headers["x-forwarded-for"] || req.socket.remoteAddress;
  ip = ip.split(",")[0];
  const project = await Project.findOne({
    project_name,
  });
  if (!project) {
    return res.status(404).json({
      msg: "Project not found",
    });
  }

  let event = await Event.findOne({
    event_type,
    project_id: project._id,
  });

  if (event) {
    event.counter += 1;
  } else {
    event = new Event({
      event_type,
      project_id: project._id,
    });
    project.events.push(event._id);
  }
  try {
    await project.save();
    const email = req.body.email || "";
    const logger = new Logger({
      log_ip: ip,
      email,
      event_id: event._id,
    });
    const logs = event.logs;
    logs.push(logger._id);
    event.logs = logs;
    await event.save();
    await logger.save();
    return res.status(200).json({ status: "success" });
  } catch (err) {
    return res.status(501).json({
      msg: err,
    });
  }
};

exports.getAll = async (req, res) => {
  try {
    const events = await Event.find({ project_id: req.query.id });
    return res.status(200).json({
      msg: "success",
      events,
    });
  } catch (err) {
    return res.status(501).json({
      msg: "failed",
      err,
    });
  }
};

exports.getLogs = async (req, res) => {
  const { logIds } = req.body;
  try {
    const logs = await Logger.find({ _id: { $in: logIds } });
    return res.status(200).json({
      status: "success",
      logs,
    });
  } catch (err) {
    return res.status(401).json({
      status: "failed",
      err,
    });
  }
};
